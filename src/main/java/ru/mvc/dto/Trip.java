package ru.mvc.dto;


import java.sql.Blob;
import java.sql.Timestamp;


public class Trip {

    private Integer id;
    private Timestamp DateAndTimeStart;
    private Timestamp DateAndTimeFinish;
    private Double[] coordinateStart;
    private Double[] coordinateFinish;
    private String addressStart;
    private String addressFinish;
    private Blob route;
    private Integer vacancies;
    private Double distance;
    private String  otherInformation;
    private Double[] lastCoordinate;
    private Person driverId;

    public Trip() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDateAndTimeStart() {
        return DateAndTimeStart;
    }

    public void setDateAndTimeStart(Timestamp dateAndTimeStart) {
        DateAndTimeStart = dateAndTimeStart;
    }

    public Timestamp getDateAndTimeFinish() {
        return DateAndTimeFinish;
    }

    public void setDateAndTimeFinish(Timestamp dateAndTimeFinish) {
        DateAndTimeFinish = dateAndTimeFinish;
    }

    public Double[] getCoordinateStart() {
        return coordinateStart;
    }

    public void setCoordinateStart(Double[] coordinateStart) {
        this.coordinateStart = coordinateStart;
    }

    public Double[] getCoordinateFinish() {
        return coordinateFinish;
    }

    public void setCoordinateFinish(Double[] coordinateFinish) {
        this.coordinateFinish = coordinateFinish;
    }

    public String getAddressStart() {
        return addressStart;
    }

    public void setAddressStart(String addressStart) {
        this.addressStart = addressStart;
    }

    public String getAddressFinish() {
        return addressFinish;
    }

    public void setAddressFinish(String addressFinish) {
        this.addressFinish = addressFinish;
    }

    public Blob getRoute() {
        return route;
    }

    public void setRoute(Blob route) {
        this.route = route;
    }

    public Integer getVacancies() {
        return vacancies;
    }

    public void setVacancies(Integer vacancies) {
        this.vacancies = vacancies;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    public void setOtherInformation(String otherInformation) {
        this.otherInformation = otherInformation;
    }

    public Double[] getLastCoordinate() {
        return lastCoordinate;
    }

    public void setLastCoordinate(Double[] lastCoordinate) {
        this.lastCoordinate = lastCoordinate;
    }

    public Person getDriverId() {
        return driverId;
    }

    public void setDriverId(Person driverId) {
        this.driverId = driverId;
    }

}
