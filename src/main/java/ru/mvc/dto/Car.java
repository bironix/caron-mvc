package ru.mvc.dto;

import java.sql.Blob;


public class Car {

    private Integer id;
    private String  carMake;
    private String  modelOfCar;
    private String  registerSign;
    private String  color;
    private Integer  seats;
    private Blob avatar;
    private Person driverId;

    public Car() {
    }

    public Car(Integer id, String carMake, String modelOfCar, String registerSign, String color, Integer seats, Blob avatar, Person driverId) {
        this.id = id;
        this.carMake = carMake;
        this.modelOfCar = modelOfCar;
        this.registerSign = registerSign;
        this.color = color;
        this.seats = seats;
        this.avatar = avatar;
        this.driverId = driverId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    public String getModelOfCar() {
        return modelOfCar;
    }

    public void setModelOfCar(String modelOfCar) {
        this.modelOfCar = modelOfCar;
    }

    public String getRegisterSign() {
        return registerSign;
    }

    public void setRegisterSign(String registerSign) {
        this.registerSign = registerSign;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Blob getAvatar() {
        return avatar;
    }

    public void setAvatar(Blob avatar) {
        this.avatar = avatar;
    }

    public Person getDriverId() {
        return driverId;
    }

    public void setDriverId(Person driverId) {
        this.driverId = driverId;
    }
}
