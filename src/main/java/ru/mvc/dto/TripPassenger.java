package ru.mvc.dto;

import java.sql.Timestamp;


public class TripPassenger {

    private Integer id;
    private Timestamp dateAndTimeStart;
    private Timestamp dateAndTimeFinish;
    private Double[] coordinateStart;
    private Double[] coordinateFinish;
    private String addressStart;
    private String addressFinish;
    private Trip tripId;
    private Person passengerId;


    public TripPassenger() {
    }

    public TripPassenger(Integer id, Timestamp dateAndTimeStart,
                         Timestamp dateAndTimeFinish, Double[] coordinateStart, Double[] coordinateFinish,
                         String addressStart, String addressFinish, Trip tripId, Person passengerId) {
        this.id = id;
        this.dateAndTimeStart = dateAndTimeStart;
        this.dateAndTimeFinish = dateAndTimeFinish;
        this.coordinateStart = coordinateStart;
        this.coordinateFinish = coordinateFinish;
        this.addressStart = addressStart;
        this.addressFinish = addressFinish;
        this.tripId = tripId;
        this.passengerId = passengerId;
    }

    public Person getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(Person passengerId) {
        this.passengerId = passengerId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDateAndTimeStart() {
        return dateAndTimeStart;
    }

    public void setDateAndTimeStart(Timestamp dateAndTimeStart) {
        this.dateAndTimeStart = dateAndTimeStart;
    }

    public Timestamp getDateAndTimeFinish() {
        return dateAndTimeFinish;
    }

    public void setDateAndTimeFinish(Timestamp dateAndTimeFinish) {
        this.dateAndTimeFinish = dateAndTimeFinish;
    }

    public Double[] getCoordinateStart() {
        return coordinateStart;
    }

    public void setCoordinateStart(Double[] coordinateStart) {
        this.coordinateStart = coordinateStart;
    }

    public Double[] getCoordinateFinish() {
        return coordinateFinish;
    }

    public void setCoordinateFinish(Double[] coordinateFinish) {
        this.coordinateFinish = coordinateFinish;
    }

    public String getAddressStart() {
        return addressStart;
    }

    public void setAddressStart(String addressStart) {
        this.addressStart = addressStart;
    }

    public String getAddressFinish() {
        return addressFinish;
    }

    public void setAddressFinish(String addressFinish) {
        this.addressFinish = addressFinish;
    }

    public Trip getTripId() {
        return tripId;
    }

    public void setTripId(Trip tripId) {
        this.tripId = tripId;
    }
}