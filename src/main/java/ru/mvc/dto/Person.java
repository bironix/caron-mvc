package ru.mvc.dto;

import java.sql.Blob;
import java.sql.Date;


public class Person {

    private Integer id;
    private String name;
    private String surname;
    private String password;
    private String email;
    private String telephoneNumber;
    private String age;
    private String  otherInformation;
    private Blob avatar;
    private Date dateOfRegistration;
    private Car car;


    public Person() {
    }

    public Person(Integer id, String name, String surname, String password, String email,
                  String telephoneNumber, String age, String otherInformation,
                  Blob avatar, Date dateOfRegistration, Car car) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.age = age;
        this.otherInformation = otherInformation;
        this.avatar = avatar;
        this.dateOfRegistration = dateOfRegistration;
        this.car = car;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    public void setOtherInformation(String otherInformation) {
        this.otherInformation = otherInformation;
    }

    public Blob getAvatar() {
        return avatar;
    }

    public void setAvatar(Blob avatar) {
        this.avatar = avatar;
    }

    public Date getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

}
